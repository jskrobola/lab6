/* Jay SKrobola
* jskrobo
* Lab5
* Lab Section: 2
* Hanjie Liu
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
Suit suit;
int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}

int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
  *  initialize the deck*/
  Card deck[52];
  for (int i = 0; i <= 52; i++) {
    if (i < 13) {
      deck[i].suit = SPADES;
      deck[i].value = i + 2;
    }
    else if (i < 26) {
      deck[i].suit = HEARTS;
      deck[i].value = i - 11;
    }
    else if (i < 39) {
      deck[i].suit = DIAMONDS;
      deck[i].value = i - 24;
    }
    else {
      deck[i].suit = CLUBS;
      deck[i].value = i -37;
    }
  }

  /*After the deck is created and initialzed we call random_shuffle() see
  the
  *  notes to determine the parameters to pass in.*/
  Card* arrayStart = &deck[0];
  Card* arrayEnd = &deck[52];

  random_shuffle(arrayStart, arrayEnd, myrandom);

  /*Build a hand of 5 cards from the first five cards of the deck created
  *  above*/
  Card hand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

  /*Sort the cards.  Links to how to call this function is in the specs
  *  provided*/
  Card* handStart = &hand[0];
  Card* handEnd = &hand[5];

  sort(handStart, handEnd, suit_order);

  /*Now print the hand below. You will use the functions get_card_name
  and
  *  get_suit_code */
  for (int i = 0; i < 5; i++) {
    cout << setw(10) << right << get_card_name(hand[i]) << " of " << get_suit_code(hand[i]) << endl;
  }

  return 0;
}

/*This function will be passed to the sort funtion. Hints on how to
implement
*  this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {

  if (lhs.suit < rhs.suit) {
    return true;
  }
  else if (lhs.suit == rhs.suit) {
    if (lhs.value < rhs.value) {
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
}

//Uses the suit and returns the correct unicode symbol
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//Returns the value in the form of string including Jack Queen and King
string get_card_name(Card& c) {
  if (c.value < 11) {
    return std::to_string(c.value);
  }
  else {
    switch (c.value) {
      case 11: return "Jack";
      case 12: return "Queen";
      case 13: return "King";
      case 14: return "Ace";
      default: return "";
    }
  }
}
